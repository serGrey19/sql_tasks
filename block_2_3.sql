SELECT  
 GROUP_CONCAT(auth.author SEPARATOR ', ') as author, books.*,
 GROUP_CONCAT(books.id SEPARATOR ', ') as id
FROM  
books LEFT JOIN auth_books  
   ON books.id = auth_books.books_id  
LEFT JOIN auth
   ON auth.id = auth_books.auth_id    
WHERE TIMESTAMPDIFF (YEAR,yearp, NOW())>10 AND TIMESTAMPDIFF (YEAR,yearp, NOW())<100
GROUP BY books.id HAVING COUNT(*) = 1;