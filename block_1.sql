/*Описание хранящихся в библиотеке книг. Включает в себя: библиотечные залы,
стеллажи, книги. Библиотечный зал описывается кратким и полным названием.
Стеллаж описывается: индексом и привязкой к залу. Книга описывается
названием, списком авторов, указанием тома или части (если есть), индексом
ISBN, годом издания, указанием индексов стеллажей и номеров полок на них,
количеством экземпляров.
b. Книги, отличающиеся только годом издания, томом или частью (или
комбинацией данных полей), имеют одинаковый ISBN. Данные книги во всех
заданиях необходимо считать одинаковыми.*/

CREATE TABLE `bibhalls` (
	`shortname` varchar(20) NOT NULL,
	`fullname` varchar(70) NOT NULL,
	PRIMARY KEY (`shortname`)
);

INSERT INTO `bibhalls` (`shortname`,`fullname`) 
VALUES ('1','First'), ('2','Second')

CREATE TABLE `racks` (
	`id` INT NOT NULL,
	`hall` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `hall` (`hall`),
	CONSTRAINT `racks_ibfk_1` FOREIGN KEY (`hall`) REFERENCES `bibhalls` (`shortname`)
);

INSERT INTO `racks` (`id`, `hall`)
VALUES ('1', '1'),('2', '1'),('3', '2'),('4', '2')

CREATE TABLE `books` (
    `id` INTEGER PRIMARY KEY,
	`title` varchar(20) NOT NULL,
	`part` INT,
    `ISBN` INT NOT NULL,
	`yearp` DATE NOT NULL
);

INSERT INTO `books` (`id`,`title`,`part`,`ISBN`,`yearp`) 
VALUES 
('1', 'lord of the rings', 1, 1000, '1954/10/10'), 
('2', 'song of ice and fire', 1, 1001, '1991/10/10'),
('3', 'miecz przeznaczenia', 2, 1003, '1992/10/10')

INSERT INTO `books` (`id`,`title`,`ISBN`,`yearp`)
VALUES 
('4','just book', 9999, '1950/12/21'),
('5','kolobok',1002, '2000/10/10'),
('6','golden egg',1004, '2000/10/10');

CREATE TABLE `auth` (
	`id` INTEGER PRIMARY KEY,
	`author` VARCHAR(70) NOT NULL
);

INSERT INTO `auth` (`id`,`author`) 
VALUES 
('1','John Ronald Reuel Tolkien'), 
('2','George Raymond Richard Martin'), 
('3','Andrzej Sapkowski'),
('4','writer'),
('5','another writer'),
('6','Иванов Иван Иванович'),
('7','Петров Пётр Петрович');

CREATE TABLE `auth_books` (
    `auth_id` INTEGER NOT NULL,
    `books_id` INTEGER NOT NULL,
    PRIMARY KEY (`auth_id`, `books_id`),
    INDEX `auth_id` (`auth_id`),
    INDEX `books_id` (`books_id`),
    CONSTRAINT `FK_auth` FOREIGN KEY (`auth_id`) 
        REFERENCES `auth` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_books` FOREIGN KEY (`books_id`) 
        REFERENCES `books` (`id`) ON DELETE CASCADE,
	`rack` INT NOT NULL,
	`shelf` INT NOT NULL,
	`coun` INT NOT NULL,
 KEY `rack` (`rack`),
 CONSTRAINT `books_ibfk_1` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`)
);

INSERT INTO `auth_books` (`auth_id`, `books_id`, `rack`, `shelf`, `coun`) VALUES ('1', '1', '1', '1', '10');

INSERT INTO `auth_books` (`auth_id`, `books_id`, `rack`, `shelf`, `coun`) VALUES ('2', '2', '2', '1', '20'), ('3', '3', '1', '1', '10'), ('6', '5', '2', '1', '5'), ('6', '6', '4', '6', '50'), ('7', '6', '4', '6', '50');